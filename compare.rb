require 'json'
require 'pry'

def parse(path)
  file = File.read(path)
  data = JSON.parse(file)

  result = {}
  data['dependency_files'].each do |dependency_file|
    path = dependency_file['path']

    deps = dependency_file['dependencies'].map do |dep|
      "#{dep['package']['name']}:#{dep['version']}"
    end

    result[path] = deps.sort
  end

  result
end

def compare(want, got)
  deps1 = parse(want)
  deps2 = parse(got)

  result = {}

  common_paths = deps1.keys & deps2.keys

  common_paths.each do |path|
    deps_want = deps1[path]
    deps_got = deps2[path]

    additional = deps_got - deps_want
    missing = deps_want - deps_got

    if additional.any? || missing.any?
      result[path] = { additional: additional, missing: missing }
    end
  end

  result
end

result = compare('want/gl-dependency-scanning-report.json', 'got/gl-dependency-scanning-report.json')

puts JSON.pretty_generate(result)
